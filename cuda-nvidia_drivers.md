# What is the minimal driver version for CUDA?

ref: https://docs.nvidia.com/cuda/cuda-toolkit-release-notes/index.html

as of 2018/09/17
```
CUDA Toolkit	Linux x86_64 Driver Version
CUDA 7.0 (7.0.28)	>= 346.46
CUDA 7.5 (7.5.16)	>= 352.31
CUDA 8.0 (8.0.44)	>= 367.48
CUDA 8.0 (8.0.61 GA2)	>= 375.26
CUDA 9.0 (9.0.76)	>= 384.81
CUDA 9.1 (9.1.85)	>= 390.46
CUDA 9.2 (9.2.88)	>= 396.26
CUDA 9.2 (9.2.148 Update 1)	>= 396.37
```
