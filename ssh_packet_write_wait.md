```
packet_write_wait: Connection to 157.99.xxx.xxx port 22: Broken pipe"
```

That is caused by the campus IT firewall killing idle sessions.
[pasteur.fr #94167] and [pasteur.fr #94405]

If you leave the ssh session idle for "sometimes" (2h11) for my lab <-> inception machines,
the campus firewall will terminate the session.

# workaround (server side): in place for the gpulab machines, needs admin privileges
```
/etc/ssh/sshd_config:
...
ClientAliveInterval 120 
...
```

# workaround (client side): in place for the gpulab machines, needs admin privileges
```
/etc/ssh/ssh_config: 
...
Host *.pasteur.fr 
        ServerAliveInterval 120 
```
# workaround (client side): for each user if you can't have it done for you 
```
~/.ssh/config: 
...
Host *.pasteur.fr 
        ServerAliveInterval 120 
```
