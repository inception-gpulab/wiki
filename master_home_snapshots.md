# what are the snapshots on $HOME (/master/home/loginname) ?

Your data on the gpulab is protect by snapshots and replication to a secondary
server. Snapshots of your $HOME are taken every hours, for 30 days which make
720 snapshots.

Any files frozen in any snapshot is accounted in your disk quota.
If you delete a file, the disk quota space used is only free when the
last snapshot containing that file is deleted... that can take up to
30 days...

# When do they expire?

720 hours (30 days) after they are taken, everytime a new snaphot is taken, the oldest one expire
automatically.

# Where are they?

- The snapshot are read-only and you just need to read them (until expired)
  from ~/.zfs/snapshot.
- There is a folder named BKP_YYYYMMDD_hhmmss for each snapshot.  e.g.
  BKP_20190201_100036 for the backup of 2019/02/01 at 10:00:36 

```
cd ~/.zfs/snapshot && /bin/ls
```

# How can you recover specific files ? 

Just go into one the snapshots and read or copy them back to your $HOME

# is it safe / can they be deleted (in case you need some emergency space)  ?                                                                                                                 
no, the end users can not delete the snapshots. 

# Emergency disk space

send an email to the admins: they can either increase your disk quota (if the storage
is not full, or manually delete snapshots).

# I had a 1000 GB disk quota, but there is only 300 GB total, why?

Remember the snapshots mentionned above? the difference between what you have and
what is "missing" are the deleted files frozen in the snapshots.

```
[tru@home ~]$ df -hP|grep AAAAA
master/home/AAAAA                307G  291G   17G  95% /master/home/AAAAA

[tru@home ~]$ sudo zfs get all master/home/AAAAA|grep -i used
master/home/AAAAA  used                  1008G                  -
master/home/AAAAA  usedbysnapshots       710G                   -
master/home/AAAAA  usedbydataset         297G                   -
master/home/AAAAA  usedbychildren        0B                     -
master/home/AAAAA  usedbyrefreservation  0B                     -
master/home/AAAAA  logicalused           1007G                  -
```

The only solution to reclaim the space is to remove the snapshots manually (ask an admin) or/and
to disable snapshots to avoid having the scratch/deleted files eating disk quota (but you loose the security).
