# monet is hosted in the gpulab setup, but is not part of the shared servers

It is also not connected through the fast/low latency infiniband network
but only though the regular gigabit ethernet, and has a full disk installation
(the gpulabXX are diskless).

Accounts on that machine are created only for the imod group.

The slurm partition name is "imod" and is also restricted to the imod
group members.
```
[tru@adm ~]$ grep imod /etc/slurm/slurm.conf
PartitionName=imod   Nodes=monet Default=NO MaxTime=INFINITE State=UP AllowAccounts=wouyang,tru,blelanda
```

```
[tru@adm ~]$ srun -w monet  hostname
srun: error: Unable to allocate resources: Requested node configuration is not available
[tru@adm ~]$ srun -p imod  -w monet  hostname
monet.imod.pasteur.fr
[tru@adm ~]$ srun -p imod hostname
monet.imod.pasteur.fr
```

