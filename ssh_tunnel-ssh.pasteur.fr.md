# reaching the internal campus machine through the bastion (ssh.pasteur.fr)

If you want to reach tars.pasteur.fr without using the campus VPN available
at https://connect.pasteur.fr, or if you want to reach your lab's desktop
from home, you can just `ssh ssh.pasteur.fr` and from there `ssh to_your_target` machine.

Or you can use a ssh tunnel (adapted from https://wiki.centos.org/TipsAndTricks/SshTips/JumpHost)
to reflect the campus setup.

example of .ssh/config (on your laptop or at home)
```
Host bastion
  Hostname ssh.pasteur.fr
  ForwardAgent yes
  User your_pasteurid_login
  ProxyCommand none

Host tars
  Hostname tars.pasteur.fr
  ProxyCommand ssh bastion -W %h:%p 2>/dev/null
  ForwardAgent yes
  User your_pasteurid_login

Host gpulab
  Hostname adm.inception.hubbioit.pasteur.fr
  ProxyCommand ssh bastion -W %h:%p 2>/dev/null
  ForwardAgent yes
  User your_gpulab_login

Host labdesktop
  Hostname LD18-nnnn.XXX.pasteur.fr
  ProxyCommand ssh bastion -W %h:%p 2>/dev/null
  ForwardAgent yes
  User your_lab_login
```

If you are using ssh keys, load them with ssh-add before starting your ssh journey!

Caveat: 
```
Reminder: X-forwarding (graphical display) is blocked by on ssh.pasteur.fr.
```
ref:
https://confluence.pasteur.fr/display/FAQA/Prerequisite+to+use+tools+with+graphical+interface


2021/06/29: use proxyjump https://confluence.pasteur.fr/display/~tru/2020/04/30/ssh+jumphost?focusedCommentId=124716348#comment-124716348
