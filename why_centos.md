# Why CentOS on the gpulab

- open source and free linux distribution
- base of the Open HPC framework (using warewulf, we could have used xcat)
- long life time (https://access.redhat.com/support/policy/updates/errata/
  https://wiki.centos.org/FAQ/General#head-fe8a0be91ee3e7dea812e8694491e1dde5b75e6d)
- I(tru) am familiar with it
- most of commercial software are certified/provided on RHEL, thus should run as is on CentOS

# other alternative
- scientific linux (another RHEL clone)
- debian
- ubuntu

# not considered
- Oracle RHEL clone  (no trusted, as could change and charge anytime)
