# Inception GPU lab
[status monitoring](/monitoring.md)

## Overview 
The GPU Lab provides computing resources that are complementary to those offered by the DSI (Direction des Systèmes Informatiques). The DSI maintains 28 scientific grade GPUs available on the tars computing cluster.

The GPU Lab currently consists of 3 servers hosting a total of 24 consumer-grade GPUs and some limited storage. These GPUs are suited to a wide range of computationally intensive tasks such as genome assembly, molecular dynamics and machine learning. 
 
The GPU Lab is an experiment and is run by a small number of scientists, unlike systems managed by DSI. The main goal is to provide a flexible environment to enable easy GPU computing. It is NOT to provide a stable and secure system. For example, do not expect your data to be safe or backed up. Please exercise common sense in using the GPU Lab resources, like avoiding endless jobs etc. Future rules of usage will be defined if necessary based on experience.



### Hardware
 * gpulab00: 8x TITAN Xp GPU cards, 512 GB of RAM, 2 sockets Intel(R) Xeon(R) CPU E5-2667 v4 @ 3.20GHz  (8 cores 25600kB of cache, HT disabled)
 * gpulab01: 8x TITAN Xp GPU cards, 128 GB of RAM, 2 sockets Intel(R) Xeon(R) CPU E5-2667 v4 @ 3.20GHz  (8 cores 25600kB of cache, HT disabled)
 * gpulab02: 8x GTX 1080Ti GPU cards, 128 GB of RAM, 2 sockets Intel(R) Xeon(R) CPU E5-2667 v4 @ 3.20GHz  (8 cores 25600kB of cache, HT disabled)
 * gpulab03: 2x A40 GPU cards (48 GB of RAM) 2 sockets Intel(R) Xeon(R) Silver 4215R CPU @ 3.20GHz (8 cores)
 * storage: 40TB SSD (NFS shared)
 * network: InfinitBand(56Gbit/s), connection to Tars cluster and the campus limited to 1 Gbit/s

### System/Software
 * [CentOS 7] (/why_centos.md)
 * Slurm (as tars) most of the informations [listed here](https://moocs.pasteur.fr/courses/Institut_Pasteur/DSI_01/1/wiki/Institut_Pasteur.DSI_01.1/command-cheatsheet/) apply.
 * Singularity (a container environment)


### Getting started
Before start, please make sure that you know how to use HPC clusters, otherwise we recommend the ["High Performance Computing Cluster" MOOC](https://moocs.pasteur.fr/courses/Institut_Pasteur/DSI_01/1/about). 
 * step 1. request an account if you don't have one: [apply](https://goo.gl/forms/xajShuv5zFk0HuT22)
 * step 2. login to the GPU lab through SSH:

```
ssh YOUR_PASTEUR_ID@adm.inception.hubbioit.pasteur.fr
```
 * step 3. run your job
  * if you are not familiar with HPC cluster, take a look at this MOOC: https://moocs.pasteur.fr/courses/Institut_Pasteur/DSI_01/1/about 
  * load environment modules or use singularity container (see below)
  * launch jobs through slurm, e.g. `srun -n1 --pty --gres=gpu:1 python xxxx.py`
 
You can also reach other machines in the GPU lab by ssh to those [hostnames](/hostnames.md)

### Connecting to the gpulab from outside

  * using only ssh through [ssh.pasteur.fr](/ssh_tunnel-ssh.pasteur.fr.md)
  * using ssh and the [campus VPN service](/campus_vpn.md)
  
### [How to use singularity](/singularity)
 * build your container
 * use your container with Slurm on the GPU nodes
 * examples:
   * Tensorflow container (https://gitlab.pasteur.fr/wouyang/singularity-tensorflow). Caveat singualrity 2.3.x, adapt for 2.4 series
   * Ubuntu container
   * Use gensoft2 and tars containers
   * Hackathon at Pasteur (https://docs.google.com/presentation/d/1ueJ7s3cxSN-n6cL5SkEeX0B03yA1CnxqA_TkTbib7-U/edit#slide=id.p)
   * very nice presentation slides https://scicore-unibas-ch.github.io/singularity-slides
   * Jean Baptiste Denis from the DSI WIP at https://confluence.pasteur.fr/display/FAQA/Singularity+-+Quickstart+-+WIP
   * Brian DuSell from ND university https://github.com/bdusell/singularity-tutorial

#### Use `module`
 * `module use /c7/shared/modulefiles` to enable the [optionnal BIS contributed software] (/c7-shared.md).
 * `module use /local/gensoft2/devmodules` or `module use /local/gensoft2/modules/` will enable the few modules copied from tars for CentOS-6
 * you can request more, but no promises

## Help

 * If you have any question, please use the chatlab to get help from other people: (<strike> https://chatlab.pasteur.fr/inception-gpulab/</strike>) https://rocketchat.pasteur.cloud/group/inception-gpulab
 * [FAQ](/FAQ.md)

## Acknowledgements

## Cite Inception Project

If you use GPU lab resources for published work, please make sure to acknowledge the Inception program (Investissement d’Avenir grant ANR-16-CONV-0005). 
In relevant presentations, please include the "investissement d'avenir" logo below:

![alt text](https://upload.wikimedia.org/wikipedia/fr/thumb/4/4a/Investissements_d%27avenir_-_logo.jpeg/220px-Investissements_d%27avenir_-_logo.jpeg)


