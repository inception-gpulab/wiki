# switch the title to GPU computing workshop in Pasteur
# Preliminary program:

## part1 presentations

 * Introduction about the GPU infrastructure in Pasteur --(by Tru)

- hardware/software available in the campus (gpulab/tars and workstation)
- what linux distribution should one use? is available/supported/...
- ie google's tensorflow is ubuntu, centos6 on tars, centos7 on gpulab,...
- do we need a linux user group here? anyone interested?
?
 * Example usage of GPU computing 
- structural biology: MD simulations speedup
(AMBER CHARMM NAMD speedup gpu vs cpu http://ambermd.org/gpus/benchmarks.htm)
- deep learning: train deep learning models for image classification, image
   segmentation and accelerating super-resolution microscopy --(by Wei)- 

 * "using singularity containers for gpu" (to be defined)  Bertrand Neron c3BI or Tru

## part2 hands on

   * Tutorial Demos? on running software in the GPU lab and tars with Singularity 
   
- JB Denis from DSI 
- Bertrand Neron c3BI
- just us ? with us and them?
- bring your own gpu software/application to install ?

   
### future

- prebuilt containers, fine tuned by NVidia (NGC)
- turn key hardware DGX station?
 
TODO: Tru-> speedup creation of accounts creation ?
