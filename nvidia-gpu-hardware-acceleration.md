# Hardware assisted acceleration for encoding and decoding video frames

ref: https://developer.nvidia.com/video-encode-decode-gpu-support-matrix

## NVENC Support Matrix
as of 2018/09/17
<table class="table table-bordered" style="font-size: 13px;">
        <thead>
          <tr class="warning">
            <th>BOARD</th>
            <th>FAMILY</th>
            <th>CHIP</th>
            <th># OF CHIPS</th>
            <th># OF NVENC<br>/CHIP</th>
            <th>Total # of NVENC</th>
            <th>Max # of concurrent sessions</th>
            <th>H.264 (AVCHD) YUV 4:2:0</th>
            <th>H.264 (AVCHD) YUV 4:4:4</th>
            <th>H.264 (AVCHD) Lossless</th>
            <th>H.265 (HEVC) 4K YUV 4:2:0</th>
            <th>H.265 (HEVC) 4K YUV 4:4:4</th>
            <th>H.265 (HEVC) 4K Lossless</th>
            <th>H.265 (HEVC) 8k</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="warning">GeForce</td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GT&nbsp;1030</td>
            <td>Pascal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>GP108&nbsp;&nbsp;&nbsp;</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1050&nbsp;/&nbsp;1050&nbsp;Ti</td>
            <td>Pascal</td>
            <td>GP107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1050&nbsp;/&nbsp;1050&nbsp;Ti</td>
            <td>Pascal</td>
            <td>GP106</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1060</td>
            <td>Pascal</td>
            <td>GP106</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1060</td>
            <td>Pascal</td>
            <td>GP104</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1070&nbsp;-&nbsp;1080</td>
            <td>Pascal</td>
            <td>GP104</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1080&nbsp;Ti</td>
            <td>Pascal</td>
            <td>GP102</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;Titan&nbsp;X&nbsp;Titan&nbsp;Xp&nbsp;&nbsp;&nbsp;</td>
            <td>Pascal</td>
            <td>GP102</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>Titan&nbsp;V</td>
            <td>Volta</td>
            <td>GV100</td>
            <td>1</td>
            <td>3</td>
            <td>3</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td class="warning">QUADRO</td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
          </tr>
          <tr>
            <td>Quadro P400&nbsp;-&nbsp;P1000</td>
            <td>Pascal</td>
            <td>GP107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>Quadro P2000</td>
            <td>Pascal</td>
            <td>GP106</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
            <tr>
            <td>Quadro P4000</td>
            <td>Pascal</td>
            <td>GP104</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>Quadro P5000</td>
            <td>Pascal</td>
            <td>GP104</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>Quadro P6000</td>
            <td>Pascal</td>
            <td>GP102</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>Quadro GP100</td>
            <td>Pascal</td>
            <td>GP100</td>
            <td>1</td>
            <td>3</td>
            <td>3</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro GV100</td>
            <td>Volta</td>
            <td>GV100</td>
            <td>1</td>
            <td>3</td>
            <td>3</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td class="warning">TESLA</td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
          </tr>
          <tr>
            <td>Tesla&nbsp;P4&nbsp;/&nbsp;P6</td>
            <td>Pascal</td>
            <td>GP104</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>Tesla P40</td>
            <td>Pascal</td>
            <td>GP102</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
           <tr>
            <td>Tesla&nbsp;P100</td>
            <td>Pascal</td>
            <td>GP100</td>
            <td>1</td>
            <td>3</td>
            <td>3</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
          </tr>
         <tr>
            <td>Tesla&nbsp;V100</td>
            <td>Volta</td>
            <td>GV100</td>
            <td>1</td>
            <td>3</td>
            <td>3</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
        </tbody>
</table>

<table class="table table-bordered" style="font-size: 13px;">
        <thead>
          <tr class="warning">
            <th>BOARD</th>
            <th>FAMILY</th>
            <th>CHIP</th>
            <th># OF CHIPS</th>
            <th># OF NVENC<br>/CHIP</th>
            <th>Total # of NVENC</th>
            <th>Max # of concurrent sessions</th>
            <th>H.264 (AVCHD) YUV 4:2:0</th>
            <th>H.264 (AVCHD) YUV 4:4:4</th>
            <th>H.264 (AVCHD) Lossless</th>
            <th>H.265 (HEVC) 4K YUV 4:2:0</th>
            <th>H.265 (HEVC) 4K YUV 4:4:4</th>
            <th>H.265 (HEVC) 4K Lossless</th>
            <th>H.265 (HEVC) 8k</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>GeForce&nbsp;GT&nbsp;630&nbsp;-&nbsp;640 GeForce&nbsp;GT&nbsp;710&nbsp;-&nbsp;730</td>
            <td>Kepler</td>
            <td>GK208&nbsp;&nbsp;</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GT&nbsp;630&nbsp;-&nbsp;640 GeForce&nbsp;GTX&nbsp;650 GeForce&nbsp;GT&nbsp;740</td>
            <td>Kepler</td>
            <td>GK107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;645&nbsp;-660&nbsp;Ti&nbsp;Boost GeForce&nbsp;GT&nbsp;740</td>
            <td>Kepler</td>
            <td>GK106</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;660&nbsp;-&nbsp;690 GeForce&nbsp;GTX&nbsp;760&nbsp;-&nbsp;770</td>
            <td>Kepler</td>
            <td>GK104</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GT&nbsp;780&nbsp;-&nbsp;780&nbsp;Ti</td>
            <td>Kepler&nbsp;(2nd&nbsp;Gen)</td>
            <td>GK110</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;Titan&nbsp;/&nbsp;Titan&nbsp;Black</td>
            <td>Kepler&nbsp;(2nd&nbsp;Gen)</td>
            <td>GK110</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;Titan&nbsp;Z</td>
            <td>Kepler&nbsp;(2nd&nbsp;Gen)</td>
            <td>GK110</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;745&nbsp;-&nbsp;750&nbsp;Ti</td>
            <td>Maxwell&nbsp;(1st&nbsp;Gen)</td>
            <td>GM107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;750 GeForce&nbsp;GTX&nbsp;950&nbsp;-&nbsp;960</td>
            <td>Maxwell&nbsp;(2nd&nbsp;Gen)</td>
            <td>GM206</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;960&nbsp;Ti&nbsp;-&nbsp;980</td>
            <td>Maxwell&nbsp;(2nd&nbsp;Gen)</td>
            <td>GM204</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;980&nbsp;Ti</td>
            <td>Maxwell&nbsp;(2nd&nbsp;Gen)</td>
            <td>GM200</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;Titan&nbsp;X</td>
            <td>Maxwell&nbsp;(2nd&nbsp;Gen)</td>
            <td>GM200</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GT&nbsp;1030</td>
            <td>Pascal</td>
            <td>GP108</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1050&nbsp;/&nbsp;1050&nbsp;Ti</td>
            <td>Pascal</td>
            <td>GP107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1050&nbsp;/&nbsp;1050&nbsp;Ti</td>
            <td>Pascal</td>
            <td>GP106</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1060</td>
            <td>Pascal</td>
            <td>GP106</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1060</td>
            <td>Pascal</td>
            <td>GP104</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1070&nbsp;-&nbsp;1080</td>
            <td>Pascal</td>
            <td>GP104</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1080&nbsp;Ti</td>
            <td>Pascal</td>
            <td>GP102</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;Titan&nbsp;X&nbsp;Titan&nbsp;Xp</td>
            <td>Pascal</td>
            <td>GP102</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>Titan&nbsp;V</td>
            <td>Volta</td>
            <td>GV100</td>
            <td>1</td>
            <td>3</td>
            <td>3</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
        </tbody>
</table>

<table class="table table-bordered" style="font-size: 13px;">
        <thead>
          <tr class="warning">
            <th>BOARD</th>
            <th>FAMILY</th>
            <th>CHIP</th>
            <th># OF CHIPS</th>
            <th># OF NVENC<br>/CHIP</th>
            <th>Total # of NVENC</th>
            <th>Max # of concurrent sessions</th>
            <th>H.264 (AVCHD) YUV 4:2:0</th>
            <th>H.264 (AVCHD) YUV 4:4:4</th>
            <th>H.264 (AVCHD) Lossless</th>
            <th>H.265 (HEVC) 4K YUV 4:2:0</th>
            <th>H.265 (HEVC) 4K YUV 4:4:4</th>
            <th>H.265 (HEVC) 4K Lossless</th>
            <th>H.265 (HEVC) 8k</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="warning">GRID</td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
          </tr>
          <tr>
            <td>GRID&nbsp;K1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>Kepler</td>
            <td>GK107</td>
            <td>4</td>
            <td>1</td>
            <td>4</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GRID K2</td>
            <td>Kepler</td>
            <td>GK104</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GRID K340</td>
            <td>Kepler</td>
            <td>GK107</td>
            <td>4</td>
            <td>1</td>
            <td>4</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GRID K520</td>
            <td>Kepler</td>
            <td>GK104</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>    
          <tr>
            <td class="warning">TESLA</td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
          </tr>
          <tr>
            <td>Tesla K10</td>
            <td>Kepler</td>
            <td>GK104</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla&nbsp;K20X</td>
            <td>Kepler</td>
            <td>GK110</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla K40</td>
            <td>Kepler</td>
            <td>GK110B</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla K80</td>
            <td>Kepler (2nd Gen)</td>
            <td>GK210</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla M10</td>
            <td>Maxwell (1st Gen)</td>
            <td>GM107</td>
            <td>4</td>
            <td>1</td>
            <td>4</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla M4</td>
            <td>Maxwell (GM206)</td>
            <td>GM206</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla M40</td>
            <td>Maxwell&nbsp;(2nd&nbsp;Gen)</td>
            <td>GM200</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla M6</td>
            <td>Maxwell&nbsp;(2nd&nbsp;Gen)</td>
            <td>GM204</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla M60</td>
            <td>Maxwell&nbsp;(2nd&nbsp;Gen)</td>
            <td>GM204</td>
            <td>2</td>
            <td>2</td>
            <td>4</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla P4</td>
            <td>Pascal</td>
            <td>GP104</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>Tesla P6</td>
            <td>Pascal</td>
            <td>GP104</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>Tesla P40</td>
            <td>Pascal</td>
            <td>GP102</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
           <tr>
            <td>Tesla&nbsp;P100</td>
            <td>Pascal</td>
            <td>GP100</td>
            <td>1</td>
            <td>3</td>
            <td>3</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
          </tr>
         <tr>
            <td>Tesla&nbsp;V100</td>
            <td>Volta</td>
            <td>GV100</td>
            <td>1</td>
            <td>3</td>
            <td>3</td>
            <td>Unrestricted</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
        </tbody>
</table>

## NVDEC Support Matrix

<table class="table table-bordered" style="font-size: 13px;">
        <thead>
          <tr class="warning">
            <th rowspan="2"><center>BOARD</center></th>
            <th rowspan="2"><center>FAMILY</center></th>
            <th rowspan="2"><center>CHIP</center></th>
            <th rowspan="2"><center># OF CHIPS</center></th>
            <th rowspan="2"><center># OF NVDEC<br>/CHIP</center></th>
            <th rowspan="2"><center>Total # of NDEC</center></th>
            <th rowspan="2"><center>MPEG-1</center></th>
            <th rowspan="2"><center>MPEG-2</center></th>
            <th rowspan="2"><center>VC-1</center></th>
            <th rowspan="2"><center>H.264<br>(AVCHD)</center></th>
            <th colspan="3"><center>H.265&nbsp;(HEVC)</center></th>
            <th rowspan="2"><center>&nbsp;&nbsp;&nbsp;VP8&nbsp;&nbsp;&nbsp;</center></th>
            <th colspan="3"><center>VP9</center></th>
          </tr>
          <tr class="warning">
            <th>8&nbsp;bit</th>
            <th>10&nbsp;bit</th>
            <th>12&nbsp;bit</th>
            <th>8&nbsp;bit</th>
            <th>10&nbsp;bit</th>
            <th>12&nbsp;bit</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>GeForce&nbsp;GT&nbsp;710&nbsp;-&nbsp;730 Geforce&nbsp;GT&nbsp;630&nbsp;-&nbsp;640</td>
            <td>Kepler</td>
            <td>GK208</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
<tr>
            <td>GeForce&nbsp;GT&nbsp;630&nbsp;-&nbsp;640 GeForce&nbsp;GTX&nbsp;650 GeForce&nbsp;GT&nbsp;740</td>
            <td>Kepler</td>
            <td>GK107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;645&nbsp;-660&nbsp;Ti&nbsp;Boost GeForce&nbsp;GT&nbsp;740</td>
            <td>Kepler</td>
            <td>GK106</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;660&nbsp;-&nbsp;690 GeForce&nbsp;GTX&nbsp;760&nbsp;-&nbsp;770</td>
            <td>Kepler</td>
            <td>GK104</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GT&nbsp;780&nbsp;-&nbsp;780&nbsp;Ti</td>
            <td>Kepler&nbsp;(2nd&nbsp;Gen)</td>
            <td>GK110</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;Titan&nbsp;/&nbsp;Titan&nbsp;Black</td>
            <td>Kepler&nbsp;(2nd&nbsp;Gen)</td>
            <td>GK110</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;Titan&nbsp;Z</td>
            <td>Kepler&nbsp;(2nd&nbsp;Gen)</td>
            <td>GK110</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;745&nbsp;-&nbsp;750&nbsp;Ti</td>
            <td>Maxwell&nbsp;(1st&nbsp;Gen)</td>
            <td>GM107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;750 GeForce&nbsp;GTX&nbsp;950&nbsp;-&nbsp;960</td>
            <td>Maxwell&nbsp;(2nd&nbsp;Gen)</td>
            <td>GM206</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;960&nbsp;Ti&nbsp;-&nbsp;980</td>
            <td>Maxwell&nbsp;(2nd&nbsp;Gen)</td>
            <td>GM204</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;980&nbsp;Ti</td>
            <td>Maxwell&nbsp;(2nd&nbsp;Gen)</td>
            <td>GM200</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;Titan&nbsp;X</td>
            <td>Maxwell&nbsp;(2nd&nbsp;Gen)</td>
            <td>GM200</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GT&nbsp;1030</td>
            <td>Pascal</td>
            <td>GP108</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1050&nbsp;/&nbsp;1050&nbsp;Ti</td>
            <td>Pascal</td>
            <td>GP107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1050&nbsp;/&nbsp;1050&nbsp;Ti</td>
            <td>Pascal</td>
            <td>GP106</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1060</td>
            <td>Pascal</td>
            <td>GP106</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1060</td>
            <td>Pascal</td>
            <td>GP104</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1070&nbsp;-&nbsp;1080</td>
            <td>Pascal</td>
            <td>GP104</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;1080&nbsp;Ti</td>
            <td>Pascal</td>
            <td>GP102</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>GeForce&nbsp;GTX&nbsp;Titan&nbsp;X&nbsp;Titan&nbsp;Xp</td>
            <td>Pascal</td>
            <td>GP102</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>Titan&nbsp;V</td>
            <td>Volta</td>
            <td>GV100</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          
        </tbody>
</table>

<table class="table table-bordered" style="font-size: 13px;">
        <thead>
          <tr class="warning">
            <th rowspan="2"><center>BOARD</center></th>
            <th rowspan="2"><center>FAMILY</center></th>
            <th rowspan="2"><center>CHIP</center></th>
            <th rowspan="2"><center># OF CHIPS</center></th>
            <th rowspan="2"><center># OF NVDEC<br>/CHIP</center></th>
            <th rowspan="2"><center>Total # of NDEC</center></th>
            <th rowspan="2"><center>MPEG-1</center></th>
            <th rowspan="2"><center>MPEG-2</center></th>
            <th rowspan="2"><center>VC-1</center></th>
            <th rowspan="2"><center>H.264<br>(AVCHD)</center></th>
            <th colspan="3"><center>H.265&nbsp;(HEVC)</center></th>
            <th rowspan="2"><center>&nbsp;&nbsp;&nbsp;VP8&nbsp;&nbsp;&nbsp;</center></th>
            <th colspan="3"><center>VP9</center></th>
          </tr>
          <tr class="warning">
            <th>8&nbsp;bit</th>
            <th>10&nbsp;bit</th>
            <th>12&nbsp;bit</th>
            <th>8&nbsp;bit</th>
            <th>10&nbsp;bit</th>
            <th>12&nbsp;bit</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Quadro&nbsp;K2000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>Kepler&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>GK107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;K2000D</td>
            <td>Kepler</td>
            <td>GK107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;K4000</td>
            <td>Kepler</td>
            <td>GK106</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;K4200</td>
            <td>Kepler</td>
            <td>GK104</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;K5000</td>
            <td>Kepler</td>
            <td>GK104</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;K5200</td>
            <td>Kepler&nbsp;(2nd Gen)</td>
            <td>GK110B</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;K6000</td>
            <td>Kepler&nbsp;(2nd Gen)</td>
            <td>GK110B</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;K620</td>
            <td>Maxwell&nbsp;(1st Gen)</td>
            <td>GM107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;K1200</td>
            <td>Maxwell&nbsp;(1st Gen)</td>
            <td>GM107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;K2200</td>
            <td>Maxwell&nbsp;(1st Gen)</td>
            <td>GM107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;M2000</td>
            <td>Maxwell&nbsp;(GM206)</td>
            <td>GM206</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;M4000</td>
            <td>Maxwell&nbsp;(2nd&nbsp;Gen)</td>
            <td>GM204</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;M5000</td>
            <td>Maxwell&nbsp;(2nd&nbsp;Gen)</td>
            <td>GM204</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;M6000</td>
            <td>Maxwell&nbsp;(2nd&nbsp;Gen)</td>
            <td>GM200</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;P400</td>
            <td>Pascal</td>
            <td>GP107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;P600</td>
            <td>Pascal</td>
            <td>GP107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;P620</td>
            <td>Pascal</td>
            <td>GP107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;P1000</td>
            <td>Pascal</td>
            <td>GP107</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;P2000</td>
            <td>Pascal</td>
            <td>GP106</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;P4000</td>
            <td>Pascal</td>
            <td>GP104</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;P5000</td>
            <td>Pascal</td>
            <td>GP104</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;P6000</td>
            <td>Pascal</td>
            <td>GP102</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
           <tr>
            <td>Quadro&nbsp;GP100</td>
            <td>Pascal</td>
            <td>GP100</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Quadro&nbsp;GV100</td>
            <td>Volta</td>
            <td>GV100</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>

        </tbody>
</table>

<table class="table table-bordered" style="font-size: 13px;">
        <thead>
          <tr class="warning">
            <th rowspan="2"><center>BOARD</center></th>
            <th rowspan="2"><center>FAMILY</center></th>
            <th rowspan="2"><center>CHIP</center></th>
            <th rowspan="2"><center># OF CHIPS</center></th>
            <th rowspan="2"><center># OF NVDEC<br>/CHIP</center></th>
            <th rowspan="2"><center>Total # of NDEC</center></th>
            <th rowspan="2"><center>MPEG-1</center></th>
            <th rowspan="2"><center>MPEG-2</center></th>
            <th rowspan="2"><center>VC-1</center></th>
            <th rowspan="2"><center>H.264<br>(AVCHD)</center></th>
            <th colspan="3"><center>H.265&nbsp;(HEVC)</center></th>
            <th rowspan="2"><center>&nbsp;&nbsp;&nbsp;VP8&nbsp;&nbsp;&nbsp;</center></th>
            <th colspan="3"><center>VP9</center></th>
          </tr>
          <tr class="warning">
            <th>8&nbsp;bit</th>
            <th>10&nbsp;bit</th>
            <th>12&nbsp;bit</th>
            <th>8&nbsp;bit</th>
            <th>10&nbsp;bit</th>
            <th>12&nbsp;bit</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="warning">GRID</td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
          </tr>
          <tr>
            <td>GRID&nbsp;K1</td>
            <td>Kepler</td>
            <td>GK107</td>
            <td>4</td>
            <td>1</td>
            <td>4</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GRID&nbsp;K2</td>
            <td>Kepler</td>
            <td>GK104</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GRID&nbsp;K340</td>
            <td>Kepler</td>
            <td>GK107</td>
            <td>4</td>
            <td>1</td>
            <td>4</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>GRID&nbsp;K520</td>
            <td>Kepler</td>
            <td>GK104</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td class="warning">TESLA</td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
            <td class="warning"></td>
          </tr>
          <tr>
            <td>Tesla&nbsp;K10</td>
            <td>Kepler</td>
            <td>GK104</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla&nbsp;K20X</td>
            <td>Kepler</td>
            <td>GK110</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla&nbsp;K40</td>
            <td>Kepler</td>
            <td>GK110B</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla&nbsp;K80</td>
            <td>Kepler&nbsp;(2nd Gen)</td>
            <td>GK210</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla&nbsp;M10</td>
            <td>Maxwell&nbsp;(1st Gen)</td>
            <td>GM107</td>
            <td>4</td>
            <td>1</td>
            <td>4</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla&nbsp;M4</td>
            <td>Maxwell&nbsp;(GM206)</td>
            <td>GM206</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla&nbsp;M6</td>
            <td>Maxwell&nbsp;(2nd Gen)</td>
            <td>GM204</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla&nbsp;M60</td>
            <td>Maxwell&nbsp;(2nd Gen)</td>
            <td>GM204</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla&nbsp;M40</td>
            <td>Maxwell&nbsp;(2nd Gen)</td>
            <td>GM200</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla&nbsp;P4&nbsp;/&nbsp;P6</td>
            <td>Pascal</td>
            <td>GP104</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla&nbsp;P40</td>
            <td>Pascal</td>
            <td>GP102</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
           <tr>
            <td>Tesla&nbsp;P100</td>
            <td>Pascal</td>
            <td>GP100</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td>NO</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Tesla&nbsp;V100</td>
            <td>Volta</td>
            <td>GV100</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
            <td class="success">YES</td>
          </tr>
         
        </tbody>
</table>
