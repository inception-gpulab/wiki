```
$ git pull
(gnome-ssh-askpass:22246): Gtk-WARNING **: cannot open display:
error: unable to read askpass response from '/usr/libexec/openssh/gnome-ssh-askpass'
```

reason:
/etc/profile.d/gnome-ssh-askpass.sh

workaround:
`unset SSH_ASKPASS`