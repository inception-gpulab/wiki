#  Machines (inception.hubbioit.pasteur.fr)
adminitrative tasks
*  adm (also login node), change your password here!
*  home (only accessible for the admins), main NFS server with SSD
*  backup (only accessible for the admins), backup of home with plain rotating disks

GPU computes nodes: internal name (infiniband network)
* gpulab00
* gpulab01
* gpulab02
* gpulab03

Pasteur campus access:
* adm.inception.hubbioit.pasteur.fr (adm)
* gpu512titanxp.inception.hubbioit.pasteur.fr (gpulab00)
* gpu128titanxp.inception.hubbioit.pasteur.fr (gpulab01)
* gpu128gtx1080ti.inception.hubbioit.pasteur.fr (gpulab02)
* ld21-1019.inception.hubbioit.pasteur.fr (gpulab03)
