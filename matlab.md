# using matlab on the gpulab (mostly interactively)

as of 2018/05/03 I have copied matlab R2016b/R2018a from tars:/local/gensoft2

- module use /local/gensoft2/modulefiles
- module av matlab

# licenses: you will be using the DSI license server
but the gpulab slurm configuration is NOT using the `-L matlab@licserv` option (https://moocs.pasteur.fr/courses/Institut_Pasteur/DSI_01/1/wiki/Institut_Pasteur.DSI_01.1/how-use-commercial-programs/#how-to-use-matlab)

updated(?)/unannounced page at https://confluence.pasteur.fr/display/FAQA/How+to+use+Matlab+on+the+cluster

# examples

- ssh to one of the campus facing node
```
[tru@sillage ~]$ ssh -X gpu128gtx1080ti.inception.hubbioit.pasteur.fr
[tru@gpulab02 ~]$ module use /local/gensoft2/modules && module add matlab && matlab
MATLAB is selecting SOFTWARE OPENGL rendering.
```

- slurm (no X11)
```
#!/bin/bash
#SBATCH -n 1
#SBATCH --gres=gpu:1
hostname
module use /local/gensoft2/modules && module add matlab
matlab  -nodesktop -nojvm -r 'rand, pause(0), exit'
```
