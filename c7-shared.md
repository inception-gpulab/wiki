# /c7/shared is the BIS contributed shared application

It is ***NOT*** defined by default (on purpose).

To enable it in the current shell:
```
$ module use /c7/shared/modulefiles
$ module av
```

To enable it "forever", just add this line in your `~/.bashrc` file:
```
[ -d  /c7/shared/modulefiles ] && module use /c7/shared/modulefiles
```

You know the drill for the module usage (same as on tars)

# examples
```
[tru@adm ~]$ module av

---------------------------- /c7/shared/modulefiles ----------------------------
   NAMD/git/2017-10-20_Linux-x86_64-multicore-CUDA
   NAMD/git/2017-10-20_Linux-x86_64-multicore                                      (D)
   NAMD/released-2.10/x86_64-ibverbs-smp-CUDA
...
   vmd/1.8.6-32bits
   vmd/1.9.2
   vmd/1.9.2-32bits
   vmd/1.9.3-cuda75
   vmd/1.9.3-cuda80
   vmd/1.9.3-opengl
   vmd/1.9.3-32bits                                                                (D)

-------------------------- /opt/ohpc/pub/modulefiles ---------------------------
   cmake/3.9.2    pmix/1.2.3

  Where:
   D:  Default Module

Use "module spider" to find all possible modules.
Use "module keyword key1 key2 ..." to search for all possible modules matching
any of the "keys".
```

# technical information

`module` is installed by `lmod`, serves the same purpose than `environment-modules` on tars. Configuration files are compatible.