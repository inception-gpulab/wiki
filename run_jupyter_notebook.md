# How to run jupyter notebook

Since all node can be reached on the campus, you can request a node with gpu and lunch a jupyter notebook.

Here is an example code:
```
srun -n1 --pty --gres=gpu:1 singularity shell --nv /master/home/wouyang/containers/ubuntu_tensorflow_1_5_0_pytorch_0_2_0_gpu.img
Singularity ubuntu_tensorflow_1_5_0_pytorch_0_2_0_gpu.img:~> hostname
gpulab00
Singularity ubuntu_tensorflow_1_5_0_pytorch_0_2_0_gpu.img:~> export XDG_RUNTIME_DIR=""  # in my case, I need this in order to start the notebook in the container.
Singularity ubuntu_tensorflow_1_5_0_pytorch_0_2_0_gpu.img:~> pip install jupyter --upgrade --user
Singularity ubuntu_tensorflow_1_5_0_pytorch_0_2_0_gpu.img:~> jupyter notebook --ip='0.0.0.0'
[I 17:05:21.989 NotebookApp] The Jupyter Notebook is running at:
[I 17:05:21.989 NotebookApp] http://localhost:8888/?token=5496246b7ad0870f5567da0074119e1a2aca17e5cf3e5f8a
```

(One important thing here is to add `--ip='*'` when starting the notebook)

Depends on the machine you get, you can find the corresponding url:
* adm.inception.hubbioit.pasteur.fr (adm)
* gpu512titanxp.inception.hubbioit.pasteur.fr (gpulab00)
* gpu128titanxp.inception.hubbioit.pasteur.fr (gpulab01)
* gpu128gtx1080ti.inception.hubbioit.pasteur.fr (gpulab02)

For the above example, I can now access the notebook with from any where on the campus: `http://gpu512titanxp.inception.hubbioit.pasteur.fr:8888/?token=5496246b7ad0870f5567da0074119e1a2aca17e5cf3e5f8a`

Be careful that anyone know this url can access your notebook on the campus.

# additionnal informations

 - 2019/02/01 see also https://confluence.pasteur.fr/display/FAQA/How+to+use+Jupyter-Notebook+on+the+cluster