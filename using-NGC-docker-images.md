# How to pull and run the NGC docker images

Since 2019/01 Nvidia and the Institut Pasteur have signed an agreement allowing us to use the [Nvidia Gpu Cloud](https://ngc.nvidia.com) NVidia-tuned/pre-built images.

You just need to register/signin and create an [API key](https://ngc.nvidia.com/configuration/api-key)

```
export SINGULARITY_DOCKER_USERNAME='$oauthtoken'
export SINGULARITY_DOCKER_PASSWORD='your_api_key'
singularity build your_name.simg docker://nvcr.io/hpc/whatever:tagged
```

example for [gromacs](https://ngc.nvidia.com/catalog/containers/hpc%2Fgromacs)
```
singularity build gromacs-2016.4.simg docker://nvcr.io/hpc/gromacs:2016.4

```

for [tensorflow](https://ngc.nvidia.com/catalog/containers/nvidia%2Ftensorflow):
```
singularity build tensorflow-18.12-py3.simg docker://nvcr.io/nvidia/tensorflow:18.12-py3
```
