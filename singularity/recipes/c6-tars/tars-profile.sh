#!/bin/sh
# 
PATH=/usr/sbin:/sbin:/usr/bin:/bin
d=$HOME/`hostname`.`date --iso-8601=minutes| sed 's/:/h/g'`-profile.tar.tgz
echo ${d}
tar \
-czvf $d \
-C / \
/etc/profile.d/pasteur_modules* \
/local/gensoft2/adm/etc/profile.d/modules* \
/local/gensoft2/adm/Modules

