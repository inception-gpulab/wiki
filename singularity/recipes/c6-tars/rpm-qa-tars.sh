#!/bin/sh
# 
PATH=/usr/sbin:/sbin:/usr/bin:/bin
d=`hostname`.`date --iso-8601=minutes| sed 's/:/h/g'`
echo ${d}
rpm -qa --queryformat='%{name}.%{arch}\n' | sort |  grep -v ^gpg-pubkey  > ${d}.rpms
echo stripping
cp ${d}.rpms ${d}.rpms.stripped
for i in \
xorg-x11-drv-nvidia-gl.x86_64 \
xorg-x11-drv-nvidia-libs.x86_64 \
nvidia-kmod.x86_64 \
xorg-x11-drv-nvidia.x86_64 \
xorg-x11-drv-nvidia-devel.x86_64 \
kernel.x86_64 \
kernel-devel.x86_64 \
kernel-firmware.noarch \
dracut-kernel.noarch \
dhclient.x86_64 \
sendmail.x86_64 \
openssh-server.x86_64 \
munge.x86_64 \
munge-libs.x86_64 \
ganglia.x86_64 \
ganglia-gmond.x86_64 \
vmware-tools \
usermode.x86_64 \
gdb.x86_64 

do
echo "$i"
sed -i -e "/^$i/d" ${d}.rpms.stripped
done

#printenv > ${d}.printenv

