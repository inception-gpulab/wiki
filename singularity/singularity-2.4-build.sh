#!/bin/sh
umask 0022
IMG_DIR=/master/home/${USER}/singularity.d
_usage() {
echo $0 _NAME
echo layout:
echo definition file: '/master/home/tru/singularity.d/recipes/${_NAME}/Singularity'
echo container: ${IMG_DIR}/containers/${_NAME}-YYYY-MM-DD-hhmm.simg
exit 1
}
if [ $# -ne 1 ]; then
_usage
fi

_NAME=$1
IMG_FILE=${IMG_DIR}/containers/${_NAME}-`date '+%Y-%m-%d-%H%M'`.simg
IMG_DEF=${IMG_DIR}/recipes/${_NAME}/Singularity
IMG_LOG=${IMG_DIR}/recipes/${_NAME}/build.log
if [ -f ${IMG_DEF} ]; then
echo building ${IMG_FILE} from ${IMG_DEF}
\rm -f ${IMG_FILE}
sudo singularity build ${IMG_FILE} ${IMG_DEF} 2>&1 | tee ${IMG_LOG}-`date '+%Y-%m-%d-%H%M'`
fi
