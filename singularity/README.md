# singularity (on the gpulab)

- build and test on the gpulab or your own lab machine
- copy the container/recipe to the gpulab or re-build it there
- copy the container to tars (with fex/scp/rsync/...)
- run on tars for "production" (mostly for larger disk storage)
- run a tars compute node singularity container on the gpulab machines with the local (partial copy) of /local/gensoft2

additionnaly
- share the recipes on the singularity hub (https://singularity-hub.org/)
- profit

# getting started

## check if you can run singularity with sudo (only required to build containers images, you don't need sudo to run a container)
(/etc/sudoers.d/singularity file on adm and home)
```
[tru@adm ~]$ sudo -l
...
    (root) NOPASSWD: /usr/bin/singularity
...

```

## running your first container from the singularity hub (a place to share your container images)
```
[tru@adm ~]$ singularity run shub://truatpasteurdotfr/singularity-alpine

```

- you are running the default runscript (here /bin/sh) inside an alpine linux toy container (mounted read-only)
- only your $HOME is available (and writable)
- a copy of the container will be saved in the current directory as `truatpasteurdotfr-singularity-alpine-master.simg`

## shell in (remember it is read-only) 

```
[tru@adm ~]$ singularity shell -w  shub://truatpasteurdotfr/singularity-alpine 
ERROR  : Unable to open squashfs image in read-write mode: Read-only file system
ABORT  : Retval = 255
[tru@adm ~]$ singularity shell  shub://truatpasteurdotfr/singularity-alpine 
Singularity: Invoking an interactive shell within container...
Singularity> 
```

- pull the container and rename it on the fly, just exec the container to use it

```
[tru@adm ~]$ singularity pull --name alpine.simg shub://truatpasteurdotfr/singularity-alpine
[tru@adm ~]$ ./alpine.simg
```

## run the container via srun:
```
[tru@adm ~]$ srun --pty singularity run shub://truatpasteurdotfr/singularity-alpine
This is what happens when you run the container...
Singularity> df /
Filesystem           1K-blocks      Used Available Use% Mounted on
none                      1024         0      1024   0% /
Singularity> hostname
gpulab01
```

## running a tars container with the gpulab partial copy of /local/gensoft2 (-B /local/gensoft2)
```
[tru@adm ~]$ srun --pty singularity run --nv -B /local/gensoft2 ~tru/singularity.d/containers/singularity-2.2-tars-nvidia-367.57.img
WARNING: Library bind directory not present in container, update container
This is what happens when you run the container...
[tru@gpulab01 ~]$ more /etc/issue
CentOS release 6.8 (Final)
Kernel \r on an \m
[tru@gpulab01 ~]$ module use /local/gensoft2/modules/
[tru@gpulab01 ~]$ module av

--------------------------------------------------------------------------------------------- /local/gensoft2/modules/ ---------------------------------------------------------------------------------------------
tensorflow/1.0.0-py2 tensorflow/1.0.0-py3

-------------------------------------------------------------------------------------------- /local/gensoft2/devmodules --------------------------------------------------------------------------------------------
atlas/3.10.2  cuda/8.0.0    cudnn/v5      gcc/4.9.0     Python/2.7.11 Python/3.6.0
```

## singularity and GPU access (--nv flag)
```
[tru@adm ~]$ srun --gres=gpu:1 singularity run --nv ~tru/singularity.d/containers/tensorflow-gpu-py3-2017-10-16-1611.img -c 'from tensorflow.python.client import device_lib; print(device_lib.list_local_devices())'
Running python3 from the container
2017-12-18 21:06:02.667703: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2017-12-18 21:06:02.667809: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2017-12-18 21:06:02.667837: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2017-12-18 21:06:02.667858: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2017-12-18 21:06:02.667884: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2017-12-18 21:06:03.222692: I tensorflow/core/common_runtime/gpu/gpu_device.cc:955] Found device 0 with properties: 
name: TITAN Xp
major: 6 minor: 1 memoryClockRate (GHz) 1.582
pciBusID 0000:06:00.0
Total memory: 11.90GiB
Free memory: 11.74GiB
2017-12-18 21:06:03.222796: I tensorflow/core/common_runtime/gpu/gpu_device.cc:976] DMA: 0 
2017-12-18 21:06:03.222821: I tensorflow/core/common_runtime/gpu/gpu_device.cc:986] 0:   Y 
2017-12-18 21:06:03.222844: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1045] Creating TensorFlow device (/gpu:0) -> (device: 0, name: TITAN Xp, pci bus id: 0000:06:00.0)
[name: "/cpu:0"
device_type: "CPU"
memory_limit: 268435456
locality {
}
incarnation: 15246849707774555536
, name: "/gpu:0"
device_type: "GPU"
memory_limit: 11972630938
locality {
  bus_id: 1
}
incarnation: 16189664473527861853
physical_device_desc: "device: 0, name: TITAN Xp, pci bus id: 0000:06:00.0"
]

```

## building your ubuntu container 
- from the command line
```
[tru@adm ~]$ wget https://gitlab.pasteur.fr/inception-gpulab/wiki/raw/master/singularity/recipes/ubuntu-xenial/Singularity
[tru@adm ~]$ sudo singularity build my_xenial.simg Singularity
```

- with a [quick and dirty script] (singularity/singularity-2.4-build.sh) (assuming you have the same layout):
```
[tru@adm ~]$ tree singularity.d
singularity.d
├── containers
│   ├── alpine-2017-10-18-0850.simg
...
│   ├── singularity-2.2-tars-nvidia-367.57.img
│   ├── tensorflow-gpu-py3-2017-10-18-0825.simg
...
├── recipes
    ├── alpine
    │   └── Singularity
...
    ├── ubuntu-xenial
    │   └── Singularity
...
[tru@adm ~]$ mkdir -p ~/singularity.d/{containers,recipes/ubuntu-xenial}
[tru@adm ~]$ cd ~/singularity.d/recipes/ubuntu-xenial && wget https://gitlab.pasteur.fr/inception-gpulab/wiki/raw/master/singularity/recipes/ubuntu-xenial/Singularity
[tru@adm ubuntu-xenial]$ wget https://gitlab.pasteur.fr/inception-gpulab/wiki/raw/master/singularity/singularity-2.4-build.sh ; chmod +x singularity-2.4-build.sh 
[tru@adm ubuntu-xenial]$ ./singularity-2.4-build.sh ubuntu-xenial
```

## a complete example for Tensorflow: (https://gitlab.pasteur.fr/wouyang/singularity-tensorflow)

# references
- singularity home page: http://singularity.lbl.gov/
- singularity on github: https://github.com/singularityware/singularity/
- https://zonca.github.io/2017/11/modify-singularity-images.html
- https://hpc.nih.gov/apps/singularity.html
- http://singularity.lbl.gov/user-guide
