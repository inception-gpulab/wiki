Sometimes, tensorflow bails out with this cryptic error: "CUDA_ERROR_UNKNOWN"
```
2018-04-18 14:15:24.024324: E tensorflow/stream_executor/cuda/cuda_driver.cc:406] failed call to cuInit: CUDA_ERROR_UNKNOWN
2018-04-18 14:15:24.024412: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:158] retrieving CUDA diagnostic information for host: monet.imod.pasteur.fr
2018-04-18 14:15:24.024438: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:165] hostname: monet.imod.pasteur.fr
2018-04-18 14:15:24.024514: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:189] libcuda reported version is: Invalid argument: expected %d.%d, %d.%d.%d, or %d.%d.%d.%d form for driver version; got "1"
2018-04-18 14:15:24.025442: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:369] driver version file contents: """NVRM version: NVIDIA UNIX x86_64 Kernel Module  390.48  Thu Mar 22 00:42:57 PDT 2018
GCC version:  gcc version 4.8.5 20150623 (Red Hat 4.8.5-16) (GCC)
"""
2018-04-18 14:15:24.025503: I tensorflow/stream_executor/cuda/cuda_diagnostics.cc:193] kernel reported version is: 390.48.0
```

# reproducer:
```
python -c 'import tensorflow as tf; tf.Session()'
```
# solution:
```
module use /c7/shared/modulefiles
module add cuda && deviceQuery
```
