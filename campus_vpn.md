# Campus VPN [reference](http://webcampus.pasteur.fr/jcms/c_524335/en/logiciel-d-acces-distant-vpn)

* connect to the campus network through the VPN
* since 2021/06/28 ~~do everything as you would be doing on the campus except that only ssh/http/https are allowed between you machine and the gpulab machines~~ ssh only through ssh.pasteur.fr 

- You can NOT connect directly to http(s)://*.hubbioit.pasteur.fr:NNNNN NNNN being a random number as in a Jupyter notebook.

