# FAQ

Sort of... if you ask a question, please summarise it and put an entry here. Thanks

- [2 way synchonisation with tars](/2_way_sync_between_tars_and_gpulab.md)
- [disable terminal/screen header changes when 'cd'](/disable_dynamic_title.md)
- [disable the GUI for ssh keys when $DISPLAY is not defined](/gnome-ssh-askpass_with_git.md)
- [sshfs for tars](/sshfs_mount_tars.md)
- [how to start a jupyter notebook](/run_jupyter_notebook.md)
- [tensorflow/CUDA_ERROR_UNKNOWN](/libcuda_reported_version_is_Invalid_argument.md)
- [matlab](/matlab.md)
- [NGC](/using-NGC-docker-images.md)
- [snapshots for gpulabs' $HOME](/master_home_snapshots.md)
- [packet_write_wait.. Broken pipe error](/ssh_packet_write_wait.md)
- [ssh tunneling through ssh.pasteur.fr](/ssh_tunnel-ssh.pasteur.fr.md)
- [VPN and gpulab](/campus_vpn.md)
- [how can I use monet](/slurm-on-monet.md)
- [hostnames of gpulabXX](/hostnames.md)
