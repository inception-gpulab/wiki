# you can mount tars through sshfs from the gpulab machines 

2019/09/06: you can use sftpcampus instead of tars to mount your /pasteur/homes or /pasteur/projets space
https://confluence.pasteur.fr/pages/viewpage.action?pageId=84509053

```
[tru@adm ~]$ mkdir sshfs/tars
[tru@adm ~]$ sshfs tars.pasteur.fr:. sshfs/tars
tru@tars.pasteur.fr's password:
[tru@adm ~]$ df
...
tars.pasteur.fr:.              3159049502208 2770170814464 375610056192  89% /master/home/tru/sshfs/tars

umounting with:
[tru@adm ~]$ fusermount -u sshfs/tars/

```

caveat:
- only one mount from any of the gpulab machine to tars at the same time
(you can not mount from adm.inception and access it from gpulab00)
- ssh + fuse are used -> limited performance expected

# CAVEAT campus IT firewall might kill idle sessions

if you leave the ssh session idle for "sometimes" (2h11) for my lab <-> inception machines,
the campus firewall will terminate the session. If you are lucky you have the following error message:
```
packet_write_wait: Connection to 157.99.xxx.xxx port 22: Broken pipe"
```
sshfs/fuse might just stall (df blocked): try to kill the sshfs/ssh process or ask for help.

