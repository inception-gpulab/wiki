# unison can be used to make a 2 way sync between tars:/pasteur/projects/XXX or tars:/pasteur/homes

- example of ~/.unison/tars-home.prf config file

```
root=/master/home/tru/
root=ssh://tars.pasteur.fr//pasteur/homes/tru/

# dont take the snapshots on both sides                                                                                                                                                                             
ignore = Path {.snapshot}
ignore = Path {.zfs}
# ignore more paths                                                                                                                                                                                                 
ignore = Path {lost+found}

# relative to the top_folder                                                                                                                                                                                        
# only take these listed folder                                                                                                                                                                                     
# add as required                                                                                                                                                                                                   
path=python
```

- example of ~/.unison/tars-projects.prf config file

```
root=/master/home/wouyang/pasteur/projets/Imod-grenier/Will/workspace
root=ssh://tars.pasteur.fr//pasteur/projets/policy01/Imod-grenier/Will/workspace

# dont take the snapshots on both sides                                                                                                                                                                             
ignore = Path {.snapshot}
ignore = Path {.zfs}
# ignore more paths                                                                                                                                                                                                 
ignore = Path {lost+found}

# relative to the top_folder                                                                                                                                                                                        
# only take these listed folder                                                                                                                                                                                     
# add as required                                                                                                                                                                                                   
path=im2im2
```

use with `unison tars-home` or `unison tars-project`

Caveat:
- you can use ssh.pasteur.fr for /pasteur/homes
- you can NOT use ssh.pasteur.fr for pasteur/projects (no longer mounted there)
- unison is not yet installed on tars, but I (tru) can provide you a stand alone version

-~tru/.unison/gensoft2.prf file
```
root=/local/gensoft2
root=ssh://tars.pasteur.fr//local/gensoft2
# relative to the top_folder



# dont take the snapshots on both sides
ignore = Path {.snapshot}
ignore = Path {.zfs}
# ignore more paths
ignore = Path {lost+found}
# only take these listed folder
path=devmodules/Python/2.7.11 
path=devmodules/Python/3.6.0 
path=devmodules/atlas/3.10.2 
path=devmodules/cuda/8.0.0 
path=devmodules/cudnn/v5 
path=devmodules/gcc/4.9.0 
path=exe/Python/2.7.11 
path=exe/Python/3.6.0 
path=exe/cuda
path=exe/cuda 
path=exe/gcc/4.9.0 
path=exe/tensorflow/1.0.0-py2 
path=exe/tensorflow/1.0.0-py3 
path=lib/atlas/3.10.2 
path=lib/cudnn/v5 
path=modules/tensorflow/1.0.0-py2 
path=modules/tensorflow/1.0.0-py3 
# /local/gensoft2/exe/tensorflow/1.0.0-py3 needs gcc/4.9.0
```
```